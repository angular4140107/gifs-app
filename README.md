# GifsApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 17.1.2.


1 - Use a loader component to wait for the api response.

2 - Use angular directives like ngIf and ngFor to add conditional functionality.

3 - In this project I use the API https://developers.giphy.com/ to extract information to the front end through a service in Angular.

4 - Use Bootstrap as a design library.

Clone the project and you can see the code.

