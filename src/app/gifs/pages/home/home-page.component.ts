import { Component } from '@angular/core';
import { SearchBoxComponent } from '../../components/search-box/search-box.component';
import { ListGifsComponent } from '../../components/list-gifs/list-gifs.component';
import { GifsService } from '../../services/gifs.service';
import { Gif } from '../../interfaces/gifs.interfaces';

@Component({
  selector: 'gifs-home-page',
  standalone: true,
  imports: [ SearchBoxComponent, ListGifsComponent ],
  templateUrl: './home-page.component.html',
})
export class HomePageComponent {


  constructor(private gifsService: GifsService) {}

  get gifs(): Gif[] {
    return this.gifsService.gifList;
  }
}
