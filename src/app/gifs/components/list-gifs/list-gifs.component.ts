import { Component, Input } from '@angular/core';
import { GifsCardComponent } from '../gifs-card/gifs-card.component';
import { Gif } from '../../interfaces/gifs.interfaces';

@Component({
  selector: 'list-gifs',
  standalone: true,
  imports: [ GifsCardComponent ],
  templateUrl: './list-gifs.component.html',
  styleUrl: './list-gifs.component.css'
})
export class ListGifsComponent {

  @Input() 
  public gifs: Gif[] = [];
}
