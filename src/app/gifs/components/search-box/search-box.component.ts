import { Component, ElementRef, ViewChild } from '@angular/core';
import { GifsService } from '../../services/gifs.service';

@Component({
  selector: 'gifs-search-box',
  standalone: true,
  imports: [],
  templateUrl: './search-box.component.html',
  styleUrl: './search-box.component.css'
})
export class SearchBoxComponent {

  @ViewChild('searchTxt')
  public searchTxt!: ElementRef<HTMLInputElement>

  constructor( private gifsService: GifsService) {}

  searchTag() {
    const newTag = this.searchTxt.nativeElement.value;
    this.gifsService.searchTag(newTag);

    this.searchTxt.nativeElement.value = '';
  }
}
