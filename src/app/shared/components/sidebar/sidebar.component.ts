import { Component } from '@angular/core';
import { GifsService } from '../../../gifs/services/gifs.service';

@Component({
  selector: 'shared-sidebar',
  standalone: true,
  imports: [],
  templateUrl: './sidebar.component.html',
  styleUrl: './sidebar.component.css'
})
export class SidebarComponent {
item: any;

  constructor(private giftService: GifsService) {
   }

   get tags(): string[] {
    return this.giftService.tagsHistory;
   }

   getGifs(tag: string):void {
    return this.giftService.searchTag(tag);
   }
}
