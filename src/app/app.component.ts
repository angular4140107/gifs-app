import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from './shared/shared.module';
import { GifsModule } from './gifs/gifs.module';

// Components
import { HomePageComponent } from './gifs/pages/home/home-page.component';
import { SidebarComponent } from './shared/components/sidebar/sidebar.component';
import { LazyImageComponent } from './shared/components/lazy-image/lazy-image.component';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [
    RouterOutlet, 
    HttpClientModule,
    GifsModule,
    SharedModule,
    SidebarComponent,
    LazyImageComponent,
    HomePageComponent,
  ],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent {
  title = 'gifs-app';
}
